package com.first;

//class block
public class Car {

	// method block
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// cntl_shift+f -->formats your code by maintaining proper indentation
		// console.log("this is to print console output in javascript");
		// in java to print on screen we need to use system and output on the screen
		System.out.println("Method block");

	}// class is loaded into ram(i.e, memory allocation) happens
		// after complete execution of main method memory allocation is cleared

}

//JavaScript is asynchronous so you need to have a control i.e, where control flow statements into picture
