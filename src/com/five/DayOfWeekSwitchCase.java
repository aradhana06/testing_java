package com.five;

public class DayOfWeekSwitchCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int day = 8; // Local Variable
		/*Switch case is expression based an not comparision based i.e, switch(expression)
		 *SWITCH creates look up table which is fastest way to retrieve data rather than a nested if's
		 *Switch does'nt do comparision and will not exit until it finds break if will not exit
		 */	
		switch (day)
		{
			case 1: 	
				System.out.println("Monday");
				break;
			case 2: 
				System.out.println("Tuesday");
				break;
			case 3: 
				System.out.println("Wednesday");
				break;
			case 4: 
				System.out.println("Thursday");
				break;
			case 5: 
				System.out.println("Friday");
				break;
			case 6: 
				System.out.println("Saturday");
				break;
			case 7: 
				System.out.println("Sunday");
				break;
			default: 
				System.out.println("Hey choose your day!!");
				break;
		}
	}

}
