package com.four;

public class Runner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int x;	
		
		//Print details of first 
		Student s = new Student(); // s- non primitive variable == Student -- non primitive datatype i.e, class == new Student() -- object
		s.name = "Ardy"; // reference variable is s i.e used to call methods variables of different  class
		s.division = 'B';
		s.rollNo = 8300;
		System.out.println(s.name + " " + s.division + "  " + s.rollNo);
		
		//Print details of second 
		Student b = new Student();
		b.name = "Bhupesh";
		b.division = 'B';
		b.rollNo = 8301;
		System.out.println(b.name + " " + b.division + "  " + b.rollNo);
		
		//Print default values of non primitive variables i.e, null/0
		Student c = new Student();
		System.out.println(c.name + " " + c.division + "  " + c.rollNo);
		
		Student d = null;// it is acceptable for non primitive variable
		//System.out.println(d.division); //this will give null pointer exception Exception in thread "main" java.lang.NullPointerException at com.four.Runner.main(Runner.java:29)
	}

}
