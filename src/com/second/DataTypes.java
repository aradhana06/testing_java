package com.second;

public class DataTypes {
//maroon color denotes java keywords
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Primitive datatype i.e, already defined data types/ they are reserved
		// keywords. also they have fixed memory size as per datatype
		int salary = 50000;
		float pi = 3.142f;
		double d = 2.333333;
		char gender = 'M';
		boolean flag = true;
		System.out.println(salary + "      s     " + pi + "       pi    " + d + "     d   " + gender + "    g      " + flag + "       f     ");

	}

}
