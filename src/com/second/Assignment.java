package com.second;

public class Assignment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		double number1, number2, result; // local variables i.e, belong to method
		number1 = 50;
		number2 = 100.00;
		result = number1 + number2;
		System.out.println("Sum of 2 numbers is: " + result);
		result = number1 - number2;
		System.out.println("Subtraction of 2 numbers is: " + result);
		result = number1 * number2;
		System.out.println("Multiplication of 2 numbers is: " + result);
		result = number1 / number2;
		System.out.println("Division of 2 numbers is: " + result);
		result = number1 % number2;
		System.out.println("Mod of 2 numbers is: " + result);

	}

}
