package com.three;

public class ArithmenticIncDecOps {

	public static void main(String[] args) {
		// TODO Auto-generated method stub	
		int a = 100;
		a = a++; //use first then increment i.e, 100
		System.out.println("use first then increment result		" + a);
		int b = ++a; //increment first then use i.e, previous value was 100 and increment first i.e, 101 and print result = 101
		System.out.println("increment first then use	" + b);
		
		a = a--; //use first then decrement i.e, 101 as previous value of a is 101
		System.out.println("use first then decrement	" + a);
		b = --a; //decrement first then use i.e, previous value was 100 and increment first i.e, 100 and print result = 100
		System.out.println("decrement first then use	" + b);


	}

}
