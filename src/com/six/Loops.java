package com.six;

public class Loops {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// Print multiplication table
		/*
		 * For loop //initializaton, comparision, increment/decrement 
		 * //never declare variable inside for loop as it has bad memory usage and avoid lengthy if else inside for loop 
		 * While loop : when there needs to dealtt with true or false we use while loop 
		 * if count comes into picture we gowith for loop 
		 *  Do while loop : irrespective of condition it will execute at least once
		 * For each: used only to traverse /read /not for writing or updating anything. it can be used only with linked list/arrays
		 * 
		 */
		int table = 5;	
		for (int i = 1; i <= 10; i++) {
		
			System.out.println("Multiplication table   " +  table * i);
		}

	}

}
