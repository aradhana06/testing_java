package com.six;

import java.util.Scanner;

public class Whileloop {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner s = new Scanner(System.in); //Scanner class to take input from user ans s is non primitive variable
		System.out.println("Enter value to multiply: ");
		int num = s.nextInt(); //returns input into int
		
		String st = s.next(); 
		char c = st.charAt(0);
		System.out.println(st);
		 
		int i = 0;
		while (i<10) {
			++i; //without i++ condition it becomes infinite loop as i will aslways be 1
			System.out.println(i*num);
		}

	}

}
