package com.browser;

public class ExecuteBrowser {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String browserName = "operA";
		if (browserName.equalsIgnoreCase("chrome")) {
			System.out.println("Launching Chrome");
		} 
		else if (browserName.equalsIgnoreCase("ie")) {
			System.out.println("Launching Internet Explorer");
		} 
		else if (browserName.equalsIgnoreCase("firefox")) {
			//Selenium code
			System.out.println("Launching Firefox");
		} 
		else {
			//Selenium clean code
			System.out.println("No browser found! Launching Chrome !!");
		} 
	}

}
