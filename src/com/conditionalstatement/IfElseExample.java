package com.conditionalstatement;

public class IfElseExample {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// check if age > 18 issue the license else not
		int age = 29;
		if (age > 18 || age == 18)  //( age == 18 -- comparator/ comparison operator not equator)
			System.out.println("issue the license");
		System.out.println("Will you print the license"); // this will not be printed as anything thats true for if condition next line will be assumed false
		
		if (age >= 18) {	
			System.out.println("issue the license with greater than or equal to operator");
		} else {
			System.out.println("don't issue the license");
		}

	}

}
