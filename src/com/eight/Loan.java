package com.eight;

public class Loan {
	//Polymorphism i.e, many forms of object or methods.
	/*
	 * Two types: 
	 * 1. Static Polymorphism i,e 
	 * 		Method overloading, 
	 * 		it uses compile time binding/early binding, 
	 * 		method is invoked using reference type
	 * Polymorphism i.e, Polymorphism to calculate loan in organization is fixed and other loan method to calculate personal loan Method overloading happens by:
	 * 1.1. differentiating the number of parameters 
	 * 1.2. Parameter data types and their sequence i.e double and int 3. method return type or access specifiers plays no role in
	 * 2. Dynamic i.e, 
	 * 		method overriding , 
	 * 		it uses late binding / run time binding, 
	 * 		method is invoked using object type
	 */
	public double calculateEMI(double principal, int time) {
		double SI = (principal * time * 5) / 100;
		double total = principal + SI;
		double emi = total / time;
		return emi;

	}
	
	public double calculateEMI(double principal, int time, double rate) {
		double SI = (principal * time * rate) / 100;
		double total = principal + SI;
		double emi = total / time;
		return emi;

	}
	

}
